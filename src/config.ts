'use strict';

export const rootApiURL = 'https://public-api.wordpress.com/rest/v1.1/sites';
export const siteName = 'en.blog';
export const numberOfPosts = 3;
