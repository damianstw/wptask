import {Component} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {LoaderService} from './loader/loader.service';
import {ApiService} from './api/api.service';

@Component({
  selector: 'app-wp',
  templateUrl: './app.component.html'
})
export class AppComponent {
  posts: any;
  comments: Array<string>;

  constructor(public loaderService: LoaderService, private http: HttpClient, private apiService: ApiService) {
    this.comments = [];
    this.apiService.fetchData('posts', '?number=3&fields=ID,title').subscribe(data => {
      try {
        this.posts = data.posts;
      } catch (error) {
        console.log(error);
      }
    });
  }

  loadComments() {
    this.comments = [];
    this.posts.some((data) => {
      this.apiService.fetchData('posts', '/' + data.ID + '/replies?number=1&fields=content').subscribe(comment => {
        try {
          this.comments.push(comment.comments[0].content);
        } catch (error) {
          console.log(error);
        }
      });
    });
  }
}
