import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as config from '../../config';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) {

  }

  fetchData(queryType, queryParams): Observable<any> {
    return this.http.get(config.rootApiURL + '/' + config.siteName + '/' + queryType + queryParams);
  }
}
